﻿using System;
namespace TestWebView
{
    public interface IClearCookies
    {
        void ClearAllCookies();
    }
}
