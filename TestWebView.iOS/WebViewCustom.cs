﻿
using TestWebView;
using TestWebView.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(MyWebView), typeof(WebViewCustom))]
namespace TestWebView.iOS
{
    public class WebViewCustom : WkWebViewRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            var weView = this;
            weView.ScrollView.Bounces = false;


            //weView.ScrollView.ContentSize = weView.Frame.Size;
            //weView.ScrollView.ScrollEnabled = true;
            //weView.ScalesPageToFit = true;
            //weView.AutoresizingMask = UIKit.UIViewAutoresizing.None;
        }




    }
}
