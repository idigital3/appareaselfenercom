﻿using System;
using Foundation;
using TestWebView.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(ClearCookies))]
namespace TestWebView.iOS
{
    public class ClearCookies : IClearCookies
    {

        public void ClearAllCookies()
        {
            NSHttpCookieStorage CookieStorage = NSHttpCookieStorage.SharedStorage;
            foreach (var cookie in CookieStorage.Cookies)
                CookieStorage.DeleteCookie(cookie);
        }
    }
}
